<div style="text-align:center;" align="center">

![Ohm Guix Channel logo](./share/ohm_channel_logo.png)

# Ohm Guix Channel

</div>

## Table of Contents:

- [Introduction](#introduction)
- [Usage](#usage)
  - [Guix channel configuration](#guix-channel-configuration)
  - [From Source Code](#from-source-code)
- [List of Packages](#packages)
  - [Desktop](#desktop)

## Introduction

This is the Ohm's collection of [GNU Guix](https://guix.gnu.org/) package derivations packaged to be used by linux-based system users.

For More informations on installing [GNU Guix](https://guix.gnu.org/) refers to the [GNU Guix Manual](https://guix.gnu.org/manual/en/html_node/Binary-Installation.html)

## Usage

### Guix Channel Configuration

The preferred way to install the channel is to configure Guix to use this channel as additional channel.
This add at your package collection the packages definited in this channel.

Create the channels configuration file at **~/.config/guix/channels.scm** and then add the snippet below:

```scheme
(cons* (channel
       (channel
       (name 'ohm)
       (url "https://gitlab.com/alex179ohm/ohm-channel.git")
       (branch "main")
       (introduction
        (make-channel-introduction
         "9136d823d4ff6f0961a700a361ee798a42662985"
         (openpgp-fingerprint "1C42 FE7F C908 97AC BBEF  37EF 3AFF F1AA 9CE9 A03B"))))
       %default-channels)
```

If your **~/.config/guix/channels.scm** file is already populated then just add the channel to your channels list:

```scheme
(cons* (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
          (openpgp-fingerprint
           "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
       (channel
       (name 'ohm)
       (url "https://gitlab.com/alex179ohm/ohm-channel.git")
       (branch "main")
       (introduction
        (make-channel-introduction
         "9136d823d4ff6f0961a700a361ee798a42662985"
         (openpgp-fingerprint "1C42 FE7F C908 97AC BBEF  37EF 3AFF F1AA 9CE9 A03B"))))
       %default-channels)
```

Pull and update your packages collection:

```sh
guix pull
guix install your_preferred_ohm_package
```

### From Source Code

If you want to just install some packages provided by this channel clone the repository:

```sh
git clone https://gitlab.com/alex179ohm/ohm-channel.git
```

Load the channel local path and install your preferred package:

```sh
guix install --load-path ./ohm-channel your_preferred_ohm_package
```

## Packages
