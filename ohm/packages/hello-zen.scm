(define-module (ohm packages hello-zen)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system cargo)
  #:use-module ((guix licenses) #:prefix license:) ;; https://github.com/drewc/guix/blob/master/guix/licenses.scm
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public hello-zen
  (package
   (name "hello-zen")
   (version "0.1")
   (home-page "https://gitlab.com/alex179ohm/hello-zen")
   (synopsis "Rust Roma Hello World Application")
   (description "This package provides an application which just print hello world on stdout")
   (build-system cargo-build-system)
   (license (list license:expat license:asl2.0))
   (source
    (origin (method url-fetch)
            (uri "https://gitlab.com/alex179ohm/hello-zen")
            (sha256
             (base32 "13pl8j463v3j01shar8l3g33bbkyavlf4i0jy3di4qa0khqil0li"))))
   (arguments
    ;; https://guix.gnu.org/manual/en/html_node/Build-Phases.html
    ;;  (delete old-phase-name)
    ;; (replace old-phase-name new-phase)
    ;; (add-before old-phase-name new-phase-name new-phase)
    ;; (add-after old-phase-name new-phase-name new-phase
      `(#:skip-build? #t))))
    ;;#:cargo-inputs ()
    ;;#:cargo-development-inputs ()
